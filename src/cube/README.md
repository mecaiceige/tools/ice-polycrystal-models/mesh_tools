# Cube with regular hexahedral mesh


`main.mshcad` is a geometry file editable with [Gmsh](https://gmsh.info/). It shows how to obtain a regular hexahedral mesh in a cube. The adjustable variables of this file are:

- `boxdim` : dimention of the cube
- `gridsize`: number of element on each edge

The exemple give 10 element on each side.

## Generate mesh

```shell
gmsh -3 cube.mshcad -format msh2 -o tmp_cube.msh
```

## Convert mesh for rheolef

```shell
msh2geo tmp_cube.msh > cube.geo
rm tmp_cube.msh
```
