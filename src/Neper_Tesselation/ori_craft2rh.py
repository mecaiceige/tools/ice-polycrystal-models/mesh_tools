#!/usr/bin/env python3
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input_file',help='input file name')
parser.add_argument('-o', '--output_file',help='output file name')

args = parser.parse_args()

adr_in=str(args.input_file)
adr_out=str(args.output_file)

value=np.loadtxt(adr_in,skiprows=10)

f = open(adr_out+'.rhori', "a")
for i in range(value.shape[0]):
    if i!=value.shape[0]-1:
        f.write('G'+str(i+1)+';'+str(value[i,2])+';'+str(value[i,3])+'\n')
    else:
        f.write('G'+str(i+1)+';'+str(value[i,2])+';'+str(value[i,3]))
f.close()