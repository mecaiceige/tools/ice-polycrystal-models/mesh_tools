#!/bin/bash

input_mesh=$1
input_rhori=$2
output_rhori=$3

output=$(grep G < $input_mesh)
filtered_output=$(echo "$output" | grep '^G[0-9]\+')

while IFS= read -r line; do
    grep -F "$line;" $input_rhori >> $output_rhori
done <<< "$filtered_output"
