#!/bin/bash

# argument
nb=$1 #number of grain
d=$2 #aspect ratio z/x1
file1=$3 #file name output


z=$(echo "scale=10; sqrt($d)" | bc)
x=$(echo "scale=10; $z/$d" | bc)
zx=$(echo "scale=10; $z*$x" | bc)

max_xz=$z
if (( $(echo "$x > $max_xz" | bc -l) )); then
    max_xz=$x
fi



# Multiply max_xz by 2
y=$(echo "scale=10; $max_xz*2" | bc)

echo "x = $x, y = $y, z = $z, area = $zx"


mkdir $file1'/'

suf_tess=".tess"
suf_msh=".msh"
suf_geo=".geo"

f_neper="_neper"
f_rheolef="_rheolef"


# generate microstructure
out=""
out+=$file1
out+=$f_neper

neper -T -n $nb -id 1 -morpho gg -format vtk,tess -domain "cube($x,$y,$z)" -tesrsize 128 -o $out

# generate mesh
in=""
in+=$file1
in+=$f_neper
in+=$suf_tess

neper -M $in -rcl "(body>0)?1:0.5" -order 1 -o $out

# Prepare msh for msh2geo
in=""
in+=$file1
in+=$f_neper
in+=$suf_msh

out=""
out+=$file1
out+=$f_rheolef
out+=$suf_msh

Neper2Rh.py -i $in -o $out

# run msh2geo
in=""
in+=$file1
in+=$f_rheolef
in+=$suf_msh

out=""
out+=$file1
out+=$f_rheolef
out+=$suf_geo

msh2geo $in > $out

# Prepare geo for rheolef
in=""
in+=$file1
in+=$f_rheolef
in+=$suf_geo

out=""
out+=$file1
out+=$f_rheolef
out+="_RH"
out+=$suf_geo


PostMesh2geo.py -i $in -o $out


in=""
in+=$file1
in+=$f_neper
in+=".vtk"

neper2craft.py $in

mv $file1"_neper_ascii.vtk" $file1"/"$file1"_craft.vtk"
mv $file1"_rheolef_RH.geo" $file1"/"$file1"_rheolef.geo"
mv $file1"_rheolef.msh" $file1"/"$file1"_gmsh.msh"

rm $file1*
