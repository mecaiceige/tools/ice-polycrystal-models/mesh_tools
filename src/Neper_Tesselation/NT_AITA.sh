#!/bin/bash

# argument
file_input=$1
file1=$2 #file name output
mr=$3 #mesh size

# extract dimention
# Read the values from lines 6 and 7 into arrays
values1=($(sed -n '6p' $file_input))
values2=($(sed -n '7p' $file_input))

# Perform the calculations
x=$(echo "${values1[0]} * ${values2[0]}" | bc)
y=$(echo "${values1[1]} * ${values2[1]}" | bc)
z=$(echo "${values1[2]} * ${values2[2]}" | bc)

# Print the results
echo "x = $x"
echo "y = $y"
echo "z = $z"


mkdir $2'/'

suf_tess=".tess"
suf_tesr=".tesr"
suf_msh=".msh"
suf_geo=".geo"

f_neper="_neper"
f_rheolef="_rheolef"


# clean microstructure
out=""
out+=$file1
out+=$f_neper

neper -T -loadtesr $file_input -transform "rmsat,grow" -o $out

# from tesr to tess file

in=""
in+=$file1
in+=$f_neper
in+=$suf_tesr

out=""
out+=$file1
out+=$f_neper

neper -T -n from_morpho -domain "cube($x,$y,$z)" -morpho "tesr:file($in)" -o $out


# generate mesh
in=""
in+=$file1
in+=$f_neper
in+=$suf_tess

out=""
out+=$file1
out+=$f_neper

neper -M $in -dim 3 -rcl $mr -mesh2dalgo fron,dela


# Prepare msh for msh2geo
in=""
in+=$file1
in+=$f_neper
in+=$suf_msh

out=""
out+=$file1
out+=$f_rheolef
out+=$suf_msh

Neper2Rh.py -i $in -o $out
# run msh2geo
in=""
in+=$file1
in+=$f_rheolef
in+=$suf_msh

out=""
out+=$file1
out+=$f_rheolef
out+=$suf_geo

msh2geo $in > $out

# Prepare geo for rheolef
in=""
in+=$file1
in+=$f_rheolef
in+=$suf_geo

out=""
out+=$file1
out+=$f_rheolef
out+="_RH"
out+=$suf_geo


PostMesh2geo.py -i $in -o $out

mv $file1"_rheolef_RH.geo" $file1"/"$file1"_rheolef.geo"
mv $file1"_rheolef.msh" $file1"/"$file1"_gmsh.msh"

rm *_neper* *.geo
