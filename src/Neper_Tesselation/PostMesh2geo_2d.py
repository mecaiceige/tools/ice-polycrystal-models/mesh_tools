#!/usr/bin/env python3
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input_file',help='input file name')
parser.add_argument('-o', '--output_file',help='output file name')

args = parser.parse_args()

adr=str(args.input_file)
adr_out=str(args.output_file)

# Load file
file1 = open(adr,'r')
Lines=file1.readlines()

# Write file
f = open(adr_out, "a")

i=0
ww=True
for line in Lines:
    if line=='domain\n':
        ww=False
        if Lines[i+1][0]=='G':
            if Lines[i+2].split()[1]=='2':
                ww=True
        elif Lines[i+1]=='top\n':
            if Lines[i+2].split()[1]=='1':
                ww=True
        elif Lines[i+1]=='bottom\n':
            if Lines[i+2].split()[1]=='1':
                ww=True
        elif Lines[i+1][0:11]=='left_bottom':
            if Lines[i+2].split()[1]=='0':
                ww=True
        elif Lines[i+1]=='right\n':
            if Lines[i+2].split()[1]=='1':
                ww=True
        elif Lines[i+1]=='left\n':
            if Lines[i+2].split()[1]=='1':
                ww=True

    if ww:
        f.write(line)
    i+=1

f.close()
