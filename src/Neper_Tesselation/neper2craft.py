#!/usr/bin/env python3
import vtk
import sys
import os
import numpy as np

for arg in sys.argv:
    print(arg)

r = vtk.vtkDataSetReader()
r.SetFileName(arg)
r.Update()

w = vtk.vtkDataSetWriter()
w.SetFileName(arg[:-4]+'_ascii.vtk')
w.SetInputData(r.GetOutput())
w.SetFileTypeToASCII()
w.Update()

com1="sed -i 's/CELL_DATA/POINT_DATA/' "+arg[:-4]+'_ascii.vtk'
os.system(com1)

com2="sed -i 's/SCALARS MaterialId short/SCALARS scalars float/' "+arg[:-4]+'_ascii.vtk'
os.system(com2)

fp = open(arg[:-4]+'_ascii.vtk')
for i, line in enumerate(fp):
    if i == 4:
        break
fp.close()

split_line=line.split()
for i in [1,2,3]:
    split_line[i]=str(int(float(split_line[i])-1))

replace_line=split_line[0]+' '+split_line[1]+' '+split_line[2]+' '+split_line[3]
com3="sed -i 's/"+line[0:-1]+"/"+replace_line+"/' "+arg[:-4]+'_ascii.vtk'
os.system(com3)


# change the line SPACING to have the same value one each pixel
with open(arg[:-4]+'_ascii.vtk') as f:
    lines = f.readlines()
    line_i = lines[5]

spa=np.round(np.mean(np.array(line_i.split()[1:]).astype(np.float64)),5)

new_line = line_i.split()[0]+' '+str(spa)+' '+str(spa)+' '+str(spa)+'\n'

lines[5] = new_line

with open(arg[:-4]+'_ascii.vtk', 'w') as f:
    f.writelines(lines)
