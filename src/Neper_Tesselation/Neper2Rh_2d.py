#!/usr/bin/env python3
import numpy as np
from itertools import permutations
from tqdm import tqdm
import argparse
import pandas as pd


parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input_file',help='input file name')
parser.add_argument('-o', '--output_file',help='output file name')

args = parser.parse_args()

adr=str(args.input_file)
adr_out=str(args.output_file)

# find max value for z direction
with open(adr) as f:
    start_marker = "$Nodes"
    for i, line in enumerate(f, 1):
        if start_marker in line:
            node_line_number = i
            break

with open(adr) as f:
    lines = f.readlines()
    line_i = int(lines[node_line_number])

data = np.loadtxt(adr, delimiter=' ', skiprows=node_line_number+1, max_rows=line_i)
z_max=np.max(data[:,3])

# Load file
file1 = open(adr,'r')
Lines=file1.readlines()
# reference point
ref_LBB=[0,0,0]

## Extract part of the mesh file to re-write it
Headline = []
PhysicalNames = []
Nodes = []
Elements=[]
Point_LBF=[]
Point_LBB=[]
Surface_Top=[]
Surface_Bottom=[]
Surface_Front=[]
Surface_Back=[]
Surface_Left=[]
Surface_Right=[]

in_sec=0
x0_w=0
x1_w=0
y0_w=0
y1_w=0
z0_w=0
z1_w=0

i=0
ix0=[]
ix1=[]
iy0=[]
iy1=[]
iEl=[]
for line in Lines:
    if (line == '$MeshFormat\n') or (in_sec==1):
        Headline.append(line)
        in_sec=1
        if line == '$EndMeshFormat\n':
            in_sec=0
    if (line == '$Nodes\n') or (in_sec==2):
        Nodes.append(line)
        in_sec=2
        if line == '$EndNodes\n':
            in_sec=0
    if (line == '$PhysicalNames\n') or (in_sec==3):
        PhysicalNames.append(line)
        in_sec=3
        if line == '$EndPhysicalNames\n':
            in_sec=0
    if (line == 'y0\n') or (in_sec==9):
        in_sec=9
        if line == 'x0\n':
            in_sec=0
        iy0.append(i)
        Surface_Bottom.append(line)
    if (line == 'x0\n') or (in_sec==10):
        in_sec=10
        if line == 'x1\n':
            in_sec=0
        ix0.append(i)
        Surface_Left.append(line)    
    if (line == 'x1\n') or (in_sec==11):
        in_sec=11
        if line == 'y1\n':
            in_sec=0
        ix1.append(i)
        Surface_Right.append(line)
    if (line == 'y1\n') or (in_sec==12):
        in_sec=12
        if line == '$EndNSets\n':
            in_sec=0
        iy1.append(i)
        Surface_Top.append(line) 
    if (line == '$Elements\n') or (in_sec==7):
        if len(line.split())>2:
            if line.split()[1]=='1':
                iEl.append(i)
        Elements.append(line)
        in_sec=7
        if line == '$EndElements\n':
            in_sec=0
    i+=1

## find id for surface
id2m=0
for nn in Elements[2:-1]:
    tmp = np.asarray(nn.split()).astype(int)
    if tmp[1]==1:
        if tmp[3] > id2m:
            id2m=tmp[3]

## Re-wrote physical name
new_PhysicalNames=[]

i=1
for nn in PhysicalNames:
    if nn.split()[0]=='2':
        new_PhysicalNames.append('2 '+nn.split()[1]+' "G'+str(i) +'"\n')
        i+=1
for nn in Nodes[2:-1]:
    tmp=np.asarray(nn.split()).astype(float)
    if (tmp[1::]==np.array(ref_LBB)).all():
        print('found it LBB',tmp[0])
        new_PhysicalNames.append('0 '+str(int(tmp[0]))+ ' "left_bottom"\n')

new_PhysicalNames.append('1 '+str(id2m+1)+' "bottom"\n')
new_PhysicalNames.append('1 '+str(id2m+2)+' "top"\n')
new_PhysicalNames.append('1 '+str(id2m+3)+' "left"\n')
new_PhysicalNames.append('1 '+str(id2m+4)+' "right"\n')

# find external surface
El2=pd.read_table(adr,delimiter=' ',skiprows=iEl[0],skipfooter=len(Lines)-iEl[-1]-1,engine='python',header=None)
npEl2=np.array(El2)[:,6::]

for i in range(len(npEl2)):
    if npEl2[i,0] in np.array(Surface_Bottom[2:-1]).astype(int) and npEl2[i,1] in np.array(Surface_Bottom[2:-1]).astype(int):
            txt=''
            for hhh in list(El2.loc[i]):
                txt=txt+str(hhh)+' '

            txt=txt[0:-1]+'\n'
            
            indices=Elements.index(txt)

            tmpE=Elements[indices].split()
            tmpE[3]=id2m+1
            tmpE[4]=id2m+1
            string2=" ".join(map(str,tmpE))
            Elements[indices]=string2+'\n'

    if npEl2[i,0] in np.array(Surface_Top[2:-1]).astype(int) and npEl2[i,1] in np.array(Surface_Top[2:-1]).astype(int):
            txt=''
            for hhh in list(El2.loc[i]):
                txt=txt+str(hhh)+' '

            txt=txt[0:-1]+'\n'
            
            indices=Elements.index(txt)

            tmpE=Elements[indices].split()
            tmpE[3]=id2m+2
            tmpE[4]=id2m+2
            string2=" ".join(map(str,tmpE))
            Elements[indices]=string2+'\n'

    if npEl2[i,0] in np.array(Surface_Left[2:-1]).astype(int) and npEl2[i,1] in np.array(Surface_Left[2:-1]).astype(int):
            txt=''
            for hhh in list(El2.loc[i]):
                txt=txt+str(hhh)+' '

            txt=txt[0:-1]+'\n'
            
            indices=Elements.index(txt)

            tmpE=Elements[indices].split()
            tmpE[3]=id2m+3
            tmpE[4]=id2m+3
            string2=" ".join(map(str,tmpE))
            Elements[indices]=string2+'\n'

    if npEl2[i,0] in np.array(Surface_Right[2:-1]).astype(int) and npEl2[i,1] in np.array(Surface_Right[2:-1]).astype(int):
            txt=''
            for hhh in list(El2.loc[i]):
                txt=txt+str(hhh)+' '

            txt=txt[0:-1]+'\n'
            
            indices=Elements.index(txt)

            tmpE=Elements[indices].split()
            tmpE[3]=id2m+4
            tmpE[4]=id2m+4
            string2=" ".join(map(str,tmpE))
            Elements[indices]=string2+'\n'


    
# write file
f = open(adr_out, "a")
for nn in Headline:
    f.write(nn)
f.write('$PhysicalNames\n')
f.write(str(len(new_PhysicalNames))+'\n')
for nn in new_PhysicalNames:
    f.write(nn)
f.write('$EndPhysicalNames\n')
for nn in Nodes:
    f.write(nn)
for nn in Elements:
    f.write(nn)   
f.close()
