#!/bin/bash

# argument
file_input=$1
file1=$2 #file name output
mr=$3


mkdir $2'/'

suf_tesr=".tesr"
suf_msh=".msh"
suf_geo=".geo"

f_neper="_neper"
f_rheolef="_rheolef"


# clean microstructure
out=""
out+=$file1
out+=$f_neper

neper -T -loadtesr $file_input -transform "rmsat,grow" -o $out


# generate mesh
in=""
in+=$file1
in+=$f_neper
in+=$suf_tesr

out=""
out+=$file1
out+=$f_neper

neper -M $in -rcl $mr -mesh2dalgo fron,dela


# Prepare msh for msh2geo
in=""
in+=$file1
in+=$f_neper
in+=$suf_msh

out=""
out+=$file1
out+=$f_rheolef
out+=$suf_msh

Neper2Rh_2d.py -i $in -o $out
# run msh2geo
in=""
in+=$file1
in+=$f_rheolef
in+=$suf_msh

out=""
out+=$file1
out+=$f_rheolef
out+=$suf_geo

msh2geo $in > $out

# Prepare geo for rheolef
in=""
in+=$file1
in+=$f_rheolef
in+=$suf_geo

out=""
out+=$file1
out+=$f_rheolef
out+="_RH"
out+=$suf_geo


PostMesh2geo_2d.py -i $in -o $out

mv $file1"_rheolef_RH.geo" $file1"/"$file1"_rheolef.geo"
mv $file1"_rheolef.msh" $file1"/"$file1"_gmsh.msh"

rm *_neper* *.geo
