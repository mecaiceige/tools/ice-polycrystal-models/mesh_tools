#!/bin/bash

# argument
nb=$1 #number of grain
file1=$2 #file name output

mkdir $2'/'

suf_tess=".tess"
suf_msh=".msh"
suf_geo=".geo"

f_neper="_neper"
f_rheolef="_rheolef"


# generate microstructure
out=""
out+=$file1
out+=$f_neper

neper -T -n $nb -dim 2 -id 1 -morpho gg -format vtk,tess -domain "square(1,2)" -tesrsize 128 -o $out

in=""
in+=$file1
in+=$f_neper
in+=$suf_tess


for i in 1
do
    
    j=$(echo "$i*0.01" | bc -l)  
    k=$(echo "$i*0.02" | bc -l) 

    in=""
    in+=$file1
    in+=$f_neper
    in+=$suf_tess   

    out=""
    out+=$file1
    out+=$f_neper
    out+=$i

    neper -T -n $nb -id 1 -morphooptiini "file($in)" -format vtk,tess -domain "cube(1,2,0$j)" -o $out

    
    # generate mesh
    out+=$suf_tess
    neper -M $out -dim 3 -order 1 -o $out -cl "0$k" -faset "faces"

    # Prepare msh for msh2geo
    in=""
    in+=$file1
    in+=$f_neper
    in+=$i
    in+=$suf_msh

    out=""
    out+=$file1
    out+=$f_rheolef
    out+=$i
    out+=$suf_msh

    Neper2Rh.py -i $in -o $out
    # run msh2geo
    in=""
    in+=$file1
    in+=$f_rheolef
    in+=$i
    in+=$suf_msh

    out=""
    out+=$file1
    out+=$f_rheolef
    out+=$i
    out+=$suf_geo

    msh2geo $in > $out

    # Prepare geo for rheolef
    in=""
    in+=$file1
    in+=$f_rheolef
    in+=$i
    in+=$suf_geo

    out=""
    out+=$file1
    out+=$f_rheolef
    out+="_RH_"
    out+=$i
    out+=$suf_geo


    PostMesh2geo.py -i $in -o $out
done

in=""
in+=$file1
in+=$f_neper
in+=".vtk"

neper2craft.py $in

mv $file1"_neper_ascii.vtk" $file1"/"$file1"_craft.vtk"
mv $file1"_rheolef_RH_1.geo" $file1"/"$file1"_rheolef.geo"
mv $file1"_rheolef1.msh" $file1"/"$file1"_gmsh.msh"

rm $file1*
